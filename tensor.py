#!/anaconda/bin/python
from __future__ import division
from __future__ import print_function
import numpy as np
import tensorflow as tf
import dmrg

if __name__ == "__main__":
    d=2; dw=3
    J=1.0; g=1.0; chi=10; L=16; N=10

    # initialization
    m_np = dmrg.initialize_matrix_product_state(chi,L)
    w_np = dmrg.initialize_matrix_product_operator(J,g,L)
    r_np = dmrg.initialize_right_environment_matrix(chi,L,m_np,w_np)
    l_np = dmrg.initialize_left_environment_matrix(chi,L)
    m = tf.get_variable("matrix_product_state",dtype=tf.float64,initializer=tf.constant(m_np))
    w = tf.get_variable("matrix_product_operator",dtype=tf.float64,initializer=tf.constant(w_np))
    r = tf.get_variable("right_environment",dtype=tf.float64,initializer=tf.constant(r_np))
    l = tf.get_variable("left_environment",dtype=tf.float64,initializer=tf.constant(l_np))

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        for i in range(1,L,1):
            h = tf.tensordot(r[i+1],w[i],axes=(0,1))
            h = tf.reshape(tf.transpose(tf.tensordot(h,l[i-1],axes=(2,0)),(2,4,0,3,5,1)),(d*chi*chi,d*chi*chi))
            eigenvalue, eigenvector = tf.self_adjoint_eig(h)
            y, x, z = tf.svd(tf.reshape(eigenvector[0],(d*chi,chi)))
            y = tf.divide(y,tf.sqrt(tf.reduce_sum(tf.multiply(y,y))))
            sess.run(tf.assign(m[i],tf.reshape(x,(d,chi,chi))))
            m_tmp = tf.tensordot(tf.diag(y),z,axes=(1,0))
            m_tmp = tf.transpose(tf.tensordot(m[i+1],m_tmp,axes=(1,1)),(0,2,1))
            sess.run(tf.assign(m[i+1],m_tmp))
            l_tmp = tf.tensordot(l[i-1],m[i],axes=(1,1))
            l_tmp = tf.tensordot(l_tmp,w[i],axes=((0,2),(0,2)))
            l_tmp = tf.transpose(tf.tensordot(l_tmp,m[i],axes=((0,3),(1,0))),(1,0,2))
            sess.run(tf.assign(l[i],l_tmp))

        for i in range(L,1,-1):
            h = tf.tensordot(r[i+1],w[i],axes=(0,1))
            h = tf.reshape(tf.transpose(tf.tensordot(h,l[i-1],axes=(2,0)),(2,4,0,3,5,1)),(d*chi*chi,d*chi*chi))
            eigenvalue, eigenvector = tf.self_adjoint_eig(h)
            eigenvector = tf.reshape(eigenvector[0],(d,chi,chi))
            eigenvector = tf.reshape(tf.transpose(eigenvector,(1,0,2)),(chi,d*chi))
            y, x, z = tf.svd(eigenvector)
            y = tf.divide(y,tf.sqrt(tf.reduce_sum(tf.multiply(y,y))))
            sess.run(tf.assign(m[i],tf.transpose(tf.reshape(z,(chi,d,chi)),(1,0,2))))
            m_tmp = tf.tensordot(x,tf.diag(y),axes=(1,0))
            sess.run(tf.assign(m[i-1],tf.tensordot(m[i-1],m_tmp,axes=(2,0))))
            r_tmp = tf.tensordot(r[i+1],m[i],axes=(2,2))
            r_tmp = tf.tensordot(r_tmp,w[i],axes=((0,2),(1,3)))
            r_tmp = tf.transpose(tf.tensordot(r_tmp,m[i],axes=((0,3),(2,0))),(1,2,0))
            sess.run(tf.assign(r[i],r_tmp))
        print(sess.run(eigenvalue[0]))



#!/anaconda/bin/python
import sys, time
import numpy as np
from scipy.sparse.linalg import eigsh

def initialize_matrix_product_state(chi,L):
    d = 2
    np.random.seed(0) 
    m = np.random.rand(L+2,d,chi,chi)
    #m = np.zeros((L+2,d,chi,chi),dtype=np.float)
    #m[1] = np.random.rand(d,chi,chi)
    return m

def initialize_matrix_product_operator(J,g,L):
    d = 2; dw = 3
    s0 = np.eye(2)
    sx = np.array([[0.,1.],[1.,0.]])
    sz = np.array([[1.,0.],[0.,-1.]])
    w = np.zeros((L+2,dw,dw,d,d),dtype=np.float)
    for i in range(L+2):
        w[i,0:,0] = [s0,sz,-g*sx]
        w[i,2,1:] = [-J*sz,s0]
    return w

def reconstruct_right_environment_matrix(i,m,w,r):
    r_tmp = np.tensordot(r[i+1],m[i],axes=(2,2))
    r_tmp = np.tensordot(r_tmp,w[i],axes=((0,2),(1,3)))
    r_tmp = np.transpose(np.tensordot(r_tmp,m[i],axes=((0,3),(2,0))),(1,2,0))
    return r_tmp

def reconstruct_left_environment_matrix(i,m,w,l):
    l_tmp = np.tensordot(l[i-1],m[i],axes=(1,1))
    l_tmp = np.tensordot(l_tmp,w[i],axes=((0,2),(0,2)))
    l_tmp = np.transpose(np.tensordot(l_tmp,m[i],axes=((0,3),(1,0))),(1,0,2))
    return l_tmp

def initialize_right_environment_matrix(chi,L,m,w):
    d = 2; dw = 3
    r = np.zeros((L+2,dw,chi,chi),dtype=np.float)
    # construct rightmost dummy r matrix
    #r[L+1,0,:,:] = 1.; r[L+1,:,0,:] = 1.; r[L+1,:,:,0] = 1.
    r[L+1,0,0,0] = 1.
    # construct each r matrix from last r iteratively
    for i in range(L,1,-1):
        r[i] = reconstruct_right_environment_matrix(i,m,w,r)
    return r

def initialize_left_environment_matrix(chi,L):
    d = 2; dw = 3
    l = np.zeros((L+2,dw,chi,chi),dtype=np.float)
    # construct leftmost dummy l matrix
    #l[0,dw-1,:,:] = 1.; l[0,:,chi-1,:] = 1.; l[0,:,:,chi-1] = 1.
    l[0,dw-1,chi-1,chi-1] = 1.
    return l

def sweep(chi,L,m,w,r,l):
    d = 2
    # right sweep
    for i in range(1,L,1):
        # construct hamiltonian
        h = np.tensordot(r[i+1],w[i],axes=(0,1))
        h = np.reshape(np.transpose(np.tensordot(h,l[i-1],axes=(2,0)),(2,4,0,3,5,1)),(d*chi*chi,d*chi*chi))

        # eigendecomposition, svd, reconstruct environment
        eigenvalue, eigenvector = eigsh(h,k=1,which='SA',return_eigenvectors=True,v0=m[i])
        x, y, z  = np.linalg.svd(np.reshape(eigenvector,(d*chi,chi)),full_matrices=0)
        y = y/np.sqrt(np.sum(y**2))
        m[i] = np.reshape(x,(d,chi,chi))
        m_tmp = np.tensordot(np.diag(y),z,axes=(1,0))
        m[i+1] = np.transpose(np.tensordot(m[i+1],m_tmp,axes=(1,1)),(0,2,1))
        l[i] = reconstruct_left_environment_matrix(i,m,w,l)

    # left sweep
    for i in range(L,1,-1):
        # construct hamiltonian
        h = np.tensordot(r[i+1],w[i],axes=(0,1))
        h = np.reshape(np.transpose(np.tensordot(h,l[i-1],axes=(2,0)),(2,4,0,3,5,1)),(d*chi*chi,d*chi*chi))

        # eigendecomposition, svd, reconstruct environment
        eigenvalue, eigenvector = eigsh(h,k=1,which='SA',return_eigenvectors=True,v0=m[i])
        eigenvector = np.reshape(eigenvector,(d,chi,chi))
        eigenvector = np.reshape(np.transpose(eigenvector,(1,0,2)),(chi,d*chi))
        x, y, z  = np.linalg.svd(eigenvector,full_matrices=0)
        y = y/np.sqrt(np.sum(y**2))
        m[i] = np.transpose(np.reshape(z,(chi,d,chi)),(1,0,2))
        m_tmp = np.tensordot(x,np.diag(y),axes=(1,0))
        m[i-1] = np.tensordot(m[i-1],m_tmp,axes=(2,0))
        r[i] = reconstruct_right_environment_matrix(i,m,w,r)
    print "energy per bound =", eigenvalue[0]/L

if __name__ == "__main__":
    start = time.time()
    J=1.0; g=1.0; chi=10; L=32; N=10
    # initialize matrices
    m = initialize_matrix_product_state(chi,L)
    w = initialize_matrix_product_operator(J,g,L)
    r = initialize_right_environment_matrix(chi,L,m,w)
    l = initialize_left_environment_matrix(chi,L)
    for i in range(N):
        sweep(chi,L,m,w,r,l)
    print "time elapsed =", time.time()-start


#!/anaconda/bin/python
import sys, time
import numpy as np
from sklearn.datasets import fetch_mldata
mnist = fetch_mldata('MNIST original')

def display_digit(some_digit):
    import matplotlib
    import matplotlib.pyplot as plt
    some_digit_image = some_digit.reshape(7,7)
    plt.imshow(some_digit_image, cmap = matplotlib.cm.binary,
               interpolation="nearest")
    plt.axis("off")
    plt.show()

def initialize_weight(d,chi,L):
    np.random.seed(0)
    weight = np.random.rand(L+2,d,chi,chi)-0.5
    #weight = np.zeros((L+2,d,chi,chi),dtype=np.float)
    return weight

def initialize_feature(d,L,n,x):
    feature = np.zeros((L+2,d,n),dtype=np.float)
    feature[1:L+1,0,:] = np.cos(np.pi/2.*x).transpose()
    feature[1:L+1,1,:] = np.sin(np.pi/2.*x).transpose()
    return feature

def initialize_label(nc,n,y):
    label = np.zeros((nc,n),dtype=np.float)
    for i in range(nc):
        label[i,:] = np.array(y==i).astype(int)
    return label

def initialize_right(d,chi,L,n,weight,feature):
    #right = np.zeros((L+2,chi,n),dtype=np.float); 
    #right[L+1,0,:] = 1.
    #for i in range(L,1,-1):
    #    right_tmp = np.tensordot(weight[i],right[i+1],axes=(2,0))
    #    # elementwise multiplication follow by reduce summation
    #    right_tmp = np.multiply(np.transpose(right_tmp,(1,0,2)),feature[i])
    #    right[i] = np.sum(right_tmp,axis=1)
    right = np.random.rand(L+2,chi,n)
    right[L+1] = 0.
    right[L+1,0,:] = 1.
    return right

def initialize_left(d,chi,L,n,weight):
    left = np.zeros((L+2,chi,n),dtype=np.float);
    left[0,chi-1,:] = 1.
    return left

def initialize_bond_tensor(d,chi,nc,weight):
    #bond = np.random.rand(nc,d,chi,chi)
    ## bond tensor of dimension (nc,d,d,chi,chi)
    #bond = np.transpose(np.tensordot(bond,weight[2],axes=(3,1)),(0,1,3,2,4))
    bond = np.random.rand(nc,d,d,chi,chi)
    return bond

def evaluate(d,chi,L,nc,n,weight,feature,bond,y):
    # reconstruct right
    right = np.zeros((L+2,chi,n),dtype=np.float);
    right[L+1,0,:] = 1.
    for i in range(L,1,-1):
        right_tmp = np.tensordot(weight[i],right[i+1],axes=(2,0))
        right_tmp = np.multiply(np.transpose(right_tmp,(1,0,2)),feature[i])
        right[i] = np.sum(right_tmp,axis=1)
    i = 1
    decision = np.tensordot(bond,right[i+2],axes=(4,0))
    decision = np.sum(np.multiply(decision,left[i-1]),axis=3)
    decision = np.sum(np.multiply(decision,feature[i+1]),axis=2)
    decision = np.sum(np.multiply(decision,feature[i]),axis=1)
    decision = np.argmax(decision,axis=0)
    print decision
    print y


def sweep(d,chi,L,nc,n,alpha,weight,feature,right,left,bond,label,flag):
    sites = range(1,L-1)
    sites.extend(range(L-1,1,-1))
    for idx, i in enumerate(sites):
    #for i in range(1,L,1):
        # elementwise multiplication follow by reduce summation
        decision = np.tensordot(bond,right[i+2],axes=(4,0))
        decision = np.sum(np.multiply(decision,left[i-1]),axis=3)
        decision = np.sum(np.multiply(decision,feature[i+1]),axis=2)
        decision = np.sum(np.multiply(decision,feature[i]),axis=1)

        # introduce None axis for outer product broadcasting
        delta_bond = label-decision
        delta_bond = delta_bond[:,None,:]*feature[i,None,:,:]
        delta_bond = delta_bond[:,:,None,:]*feature[i+1,None,:,:]
        delta_bond = delta_bond[:,:,:,None,:]*left[i-1,None,:,:]
        delta_bond = delta_bond[:,:,:,:,None,:]*right[i+1,None,:,:]
        delta_bond = np.sum(delta_bond,axis=5)
        bond = bond+alpha*delta_bond
    
        if (idx/(L-2)==0): # right sweep
            # construct new bond for the next site through svd
            bond = np.reshape(np.transpose(bond,(1,3,0,2,4)),(d*chi,nc*d*chi))
            x, y, z = np.linalg.svd(bond)
            if flag == 0: y = y[0:chi]/np.sqrt(sum(y[0:chi]**2))
            else: y = y[0:chi]
            weight[i] = np.reshape(x[0:d*chi,0:chi],(d,chi,chi))
            z = np.reshape(z[0:chi,:],(chi,nc,d,chi))
            z = np.tensordot(z,np.diag(y),axes=(0,1))
            bond = np.transpose(np.tensordot(z,weight[i+2],axes=(2,1)),(0,1,3,2,4))

            left_tmp = np.tensordot(weight[i],left[i-1],axes=(1,0))
            left_tmp = np.multiply(np.transpose(left_tmp,(1,0,2)),feature[i])
            left[i] = np.sum(left_tmp,axis=1)
        else: # left sweep
            bond = np.reshape(np.transpose(bond,(0,1,3,2,4)),(nc*d*chi,d*chi))
            x, y, z = np.linalg.svd(bond)
            if flag == 0: y = y[0:chi]/np.sqrt(sum(y[0:chi]**2))
            else: y = y[0:chi]
            weight[i+1] = np.transpose(np.reshape(z[0:chi,0:d*chi],(chi,d,chi)),(1,0,2))
            x = np.reshape(x[:,0:chi],(nc,d,chi,chi))
            x = np.tensordot(x,np.diag(y),axes=(3,0))
            bond = np.transpose(np.tensordot(x,weight[i-1],axes=(2,2)),(0,3,1,4,2))

            right_tmp = np.tensordot(weight[i+1],right[i+2],axes=(2,0))
            right_tmp = np.multiply(np.transpose(right_tmp,(1,0,2)),feature[i+1])
            right[i+1] = np.sum(right_tmp,axis=1)
        cost = np.sum((label-decision)**2)/2
        print "site cost =", i, cost



if __name__ == "__main__":
    start = time.time()
    chi=3; n=100; alpha=0.01
    L=7*7; d=2; nc=10

    x, y = mnist["data"], mnist["target"]
    # average clusters, normalization
    x = np.mean(np.reshape(x,(70000,7,4,7,4)),axis=(2,4))/255
    x = np.reshape(x,(70000,7*7))
    x_train, x_test, y_train, y_test = x[:60000], x[60000:], y[:60000], y[60000:]
    shuffle_index = np.random.permutation(60000)
    x_train, y_train = x_train[shuffle_index], y_train[shuffle_index]
    x_train, y_train = x_train[:n], y_train[:n]

    label   = initialize_label(nc,n,y_train)
    weight  = initialize_weight(d,chi,L)
    feature = initialize_feature(d,L,n,x_train)
    right   = initialize_right(d,chi,L,n,weight,feature)
    left    = initialize_left(d,chi,L,n,weight)
    bond    = initialize_bond_tensor(d,chi,nc,weight)

    sweep(d,chi,L,nc,n,alpha,weight,feature,right,left,bond,label,1)
    sweep(d,chi,L,nc,n,alpha,weight,feature,right,left,bond,label,1)

    #evaluate(d,chi,L,nc,n,weight,feature,bond,y_train)

    print "time elapsed =", time.time()-start
